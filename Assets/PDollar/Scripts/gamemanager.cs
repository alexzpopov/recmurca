﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class gamemanager : Singleton<gamemanager> {
	public float levelTime=3;
	// Use this for initialization
	public Text textLevel;
	public Text textgameover;
	public Transform butEditor;
	public InputField inputName;
	int level=0;

	public int Level {
		get {
			return level;
		}
		set {
			level = value;


		}
	}

	public GameObject show;
	public Recognizer recogn;
	float[] timesToGesture={5,5,4.5f,4.5f,4,4,3.5f};//time per Gesture
	int[] GestureinLevel={1,2,8,13,21,34,55};//how many questions
		int score=0;
	bool gameover;
	bool win;
	bool gameStatus;//

	private IEnumerator ShowLevelNFigure()
	{
		textLevel.text="Level "+(level+1);
		textLevel.gameObject.SetActive(true);
		recogn.IsReadOnly=true;
		recogn.randomSet();
		yield return new WaitForSeconds(levelTime);
     	textLevel.gameObject.SetActive(false);
		recogn.IsReadOnly=false;
		//yield return new WaitForSeconds(levelTime);
	}
	private IEnumerator ShowGameOver()
	{
		//Debug.Log("show gameover");
		textgameover.gameObject.SetActive(true);
		yield return new WaitForSeconds(levelTime);
		textgameover.gameObject.SetActive(false);
	}

	public IEnumerator doEditor()
	{
		textLevel.transform.gameObject.SetActive(false);
		butEditor.gameObject.SetActive(true);
		recogn.IsReadOnly=false;
		yield return null;
	}
	public void goStart()
	{
		ShowPanels.Instance.HideGamePanel();
		ShowPanels.Instance.ShowMenu();
		recogn.clearGest();
		//recogn.IsReadOnly=true;
	}

	public IEnumerator doStart()
	{
		textLevel.transform.gameObject.SetActive(true);
		butEditor.gameObject.SetActive(false);
		show.SetActive(true);
		level=0;
		gameover=false;

		StartCoroutine(GameLoop());
		yield return null;
	}

	private IEnumerator GameLoop()
	{

		win=true;
		score=0;
		while (!gameover)
		{
			if (win)
			{
				win=false;
				yield return StartCoroutine(ShowLevelNFigure());
				yield return StartCoroutine(LevelStart());
			}
			else
				break;
				//yield return null;
		}

//		Debug.Log("Gameover GameLoop");

		yield return StartCoroutine(ShowGameOver());
		//yield return new WaitForSeconds(levelTime);
		ShowPanels.Instance.HideGamePanel();
		ShowPanels.Instance.ShowMenu();
	}

	private IEnumerator LevelStart()
	{
	//	Debug.Log("LevelStart"+level);
		float elapsedTime;
		textLevel.transform.gameObject.SetActive(true);

		yield return new WaitForSeconds(levelTime);
		recogn.IsReadOnly=false;
		//wait

		textLevel.transform.gameObject.SetActive(false);
	//	Debug.Log("Start"+level);

			
		elapsedTime =GestureinLevel[level]*timesToGesture[level];
		while (elapsedTime >0)
		{
			elapsedTime -= Time.deltaTime;
			if (recogn.gameOver)
			{
				
				if (recogn.win)
				{
					win=true;
					score++;

//					Debug.Log("Win level "+level);
					if (score>=GestureinLevel[level])
					{
						textLevel.text="Level "+(level+1);
					level++;
						score=0;
//						Debug.Log("level"+level);
					}

					recogn.win=false;
					recogn.gameOver=false;
					break;
				}
				else{
					gameover=true;
					win=false;

	//				Debug.Log("Gameover ");
				}
				break;
			}
			yield return null;
		}
		//yield return null;
}

// Update is called once per frame
//	void Update () {
//		levelTime-=Time.deltaTime;
//		if (levelTime<=0)
//		{
//			this.gameObject.SetActive(false);
//			show.SetActive(true);
//		}
//	}

}
