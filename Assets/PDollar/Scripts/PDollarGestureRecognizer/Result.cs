﻿namespace PDollarGestureRecognizer {

	public struct Result {

		public string GestureClass;
		public float Score;
		public int PosI;
	}
}