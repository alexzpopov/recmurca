﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using PDollarGestureRecognizer;

public class Recognizer : MonoBehaviour {

	public Transform gestureOnScreenPrefab;

	public List<Gesture> trainingSet = new List<Gesture>();

	private List<Point> points = new List<Point>();
	private int strokeId = -1;

	private Vector3 virtualKeyPosition = Vector2.zero;
	private Rect drawArea;

	private RuntimePlatform platform;
	private int vertexCount = 0;

	private List<LineRenderer> gestureLinesRenderer = new List<LineRenderer>();
	private LineRenderer currentGestureLineRenderer;

	//GUI
	private string message;
	private bool recognized;
	private string newGestureName = "";
	Transform showFigures=null;
	Transform tmpGesture=null;
	/// <summary>
	/// game
	/// </summary>
	bool isReadOnly=true;
	int lastfigures=-1;
	public bool gameOver;
	public bool win;

	public bool IsReadOnly {
		get {
			return isReadOnly;
		}
		set {
			isReadOnly = value;
			if (!isReadOnly)
			if (showFigures!=null)
				Destroy(showFigures.gameObject);
			else
			{
				if (tmpGesture!=null)
					Destroy(tmpGesture.gameObject);
			}
		}
	}

	public void clearGest()
	{
		if (showFigures!=null)
			Destroy(showFigures.gameObject);
		
		if (tmpGesture!=null)
			Destroy(tmpGesture.gameObject);
		
	}
	public void randomSet()
	{
		showFigures = Instantiate(gestureOnScreenPrefab, transform.position, transform.rotation) as Transform;
		currentGestureLineRenderer = showFigures.GetComponent<LineRenderer>();

		gestureLinesRenderer.Add(currentGestureLineRenderer);
		//trainingSet[0].
		int posI=UnityEngine.Random.Range(0,trainingSet.Count);
		//posI=1;

		Debug.Log("i mean "+posI);

		Vector3 point;
		vertexCount=0;
		lastfigures=posI;

		for (int i=0;i<trainingSet[posI].Points.Length;i++)
		{
			point.x=trainingSet[posI].Points[i].X;
			point.y=trainingSet[posI].Points[i].Y*-1;
			point.z=0;
			currentGestureLineRenderer.SetVertexCount(++vertexCount);
			currentGestureLineRenderer.SetPosition(vertexCount - 1,point);

		}

	}

	void Awake () {

		platform = Application.platform;
		drawArea = new Rect(0, 0, Screen.width , Screen.height);

		//Load pre-made gestures
		TextAsset[] gesturesXml = Resources.LoadAll<TextAsset>("GestureSet/");
		foreach (TextAsset gestureXml in gesturesXml)
			trainingSet.Add(GestureIO.ReadGestureFromXML(gestureXml.text));

		//Load user custom gestures
		Debug.Log("add "+Application.persistentDataPath);
		string[] filePaths = Directory.GetFiles(Application.persistentDataPath, "*.xml");
		foreach (string filePath in filePaths)
			trainingSet.Add(GestureIO.ReadGestureFromFile(filePath));
	}

	void Update () {
		
		if (isReadOnly )
			return;
		
		if (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touchCount > 0) {
				virtualKeyPosition = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
			}
		} else {
			if (Input.GetMouseButton(0)) {
				virtualKeyPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
			}
		}

		if (drawArea.Contains(virtualKeyPosition)) {

			if (Input.GetMouseButtonDown(0)) {

				if (recognized) {

					recognized = false;
					if (gestureLinesRenderer.Count<10)
						return;
					if (gestureLinesRenderer[0]==null)
						return;
					strokeId = -1;

					points.Clear();

					foreach (LineRenderer lineRenderer in gestureLinesRenderer) {

						lineRenderer.SetVertexCount(0);
						Destroy(lineRenderer.gameObject);
					}

					gestureLinesRenderer.Clear();
				}

				++strokeId;
				
				tmpGesture = Instantiate(gestureOnScreenPrefab, transform.position, transform.rotation) as Transform;
				currentGestureLineRenderer = tmpGesture.GetComponent<LineRenderer>();
				
				gestureLinesRenderer.Add(currentGestureLineRenderer);
				
				vertexCount = 0;
			}
			
			if (Input.GetMouseButton(0)) {
				points.Add(new Point(virtualKeyPosition.x, -virtualKeyPosition.y, strokeId));

				currentGestureLineRenderer.SetVertexCount(++vertexCount);
				currentGestureLineRenderer.SetPosition(vertexCount - 1, Camera.main.ScreenToWorldPoint(new Vector3(virtualKeyPosition.x, virtualKeyPosition.y, 10)));
			}
			if (Input.GetMouseButtonUp(0)) {
				if (vertexCount<10) 
				{
					recognized=true;
					return;
				}
				isReadOnly=true;
				butRecognize();

		}
	}
	}
	public void butRecognize()
	{
		recognized = true;

		Gesture candidate = new Gesture(points.ToArray());
		Result gestureResult = PointCloudRecognizer.Classify(candidate, trainingSet.ToArray());
		gameOver=true;
		recognized = false;
		if (gestureResult.PosI==lastfigures)
		{
			win=true;
			Debug.Log("win");
		}

	}
	public void butAdd()
	{
		newGestureName=gamemanager.Instance.inputName.text;
		string fileName = String.Format("{0}/{1}-{2}.xml", Application.persistentDataPath, newGestureName, DateTime.Now.ToFileTime());

		#if !UNITY_WEBPLAYER
		GestureIO.WriteGesture(points.ToArray(), newGestureName, fileName);
		#endif

		trainingSet.Add(new Gesture(points.ToArray(), newGestureName));

		newGestureName = "";
		recognized = false;
		isReadOnly=false;
		currentGestureLineRenderer.SetVertexCount(0);
		vertexCount=0;

	}
//	void OnGUI() {
//
//		GUI.Box(drawArea, "Draw Area");
//
//		GUI.Label(new Rect(10, Screen.height - 40, 500, 50), message);
//
//		if (GUI.Button(new Rect(Screen.width - 100, 10, 100, 30), "Recognize")) {
//
//
//			
//		//	message = gestureResult.GestureClass + " " + gestureResult.Score;
//		}
//
//		GUI.Label(new Rect(Screen.width - 200, 150, 70, 30), "Add as: ");
//		newGestureName = GUI.TextField(new Rect(Screen.width - 150, 150, 100, 30), newGestureName);
//
//		if (GUI.Button(new Rect(Screen.width - 50, 150, 50, 30), "Add") && points.Count > 0 && newGestureName != "") {
//
//			string fileName = String.Format("{0}/{1}-{2}.xml", Application.persistentDataPath, newGestureName, DateTime.Now.ToFileTime());
//
//			#if !UNITY_WEBPLAYER
//				GestureIO.WriteGesture(points.ToArray(), newGestureName, fileName);
//			#endif
//
//			trainingSet.Add(new Gesture(points.ToArray(), newGestureName));
//
//			newGestureName = "";
//		}
//	}
}
